class Vehicle
	def initialize(name, price)
		@name = name
		@price = price
	end

	def show
		"Name: #{@name}, Price: #{@price}"
	end

	def price=(newprice)
		@price = newprice
	end
end

class Bike < Vehicle
	def initialize(name, price, dealer)
		super(name, price)
		@dealer = dealer
	end

	def show
		super + ", Dealer: #{@dealer}"
	end
end

# obj = Vehicle.new("Activa", "70000")
# value_vehicle = obj.show
# puts value_vehicle

bike_obj = Bike.new("KTM", 100000, "tester")
bike_obj.price = 111111
value_bike = bike_obj.show
puts value_bike


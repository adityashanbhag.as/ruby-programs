class Interest
	def initialize(&block)
    	@block = block
	end

	def compound_interest(principle, rate, time)
		val = ((1 + rate.to_f/100)**time)
		# puts "val: ", val
	    amt = principle * val 
	    # puts "amt: ", amt
	    ci = amt - principle 
	    puts "Compound interest is: #{ci}"
	    ci
	end

	def simple_interest(principle, rate, time)
	    si = (principle * time * rate.to_f)/100
	    puts "The Simple Interest is: #{si}"
	    si
	end

	def calculate(principle, time)
		rate = @block.call(principle)
		puts "rate: #{rate}"
		difference = compound_interest(principle, rate, time) - simple_interest(principle, rate, time)
		puts "Difference: #{difference}"
	end
end

puts "Enter principle amount"
principle = gets.chomp.to_i
puts "Enter time span in years"
time = gets.chomp.to_i

obj = Interest.new() { |principle| 10 % principle }
val = obj.calculate(principle, time)
puts val


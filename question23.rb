def csvreader()
	require 'csv'
	require 'active_support/inflector'

	data = CSV.read("sample.csv", headers: true)
	# data = CSV.read(File.read("sample.csv"), headers: false)
	col = data.by_col["Designation"].sort
	# print col

	unique_col = col.uniq
	finaldata = ""
	for u in unique_col
		if col.count(u) > 1
			finaldata += u.pluralize + "\n"
		else
			finaldata += u + "\n"
		end
		for d in data
			if u == d[2]
				finaldata += "#{d[0]} (EmpId}: #{d[1]}) \n"
				# data.delete(d)
				# print data
			end
		end
	end

	# CSV.open("final.csv", "w") do |csv|
 #  	csv << ["white", 2]
 #  	csv << ["black", 1]
	# end
	File.write("finaldata.txt", finaldata)
end
csvreader()

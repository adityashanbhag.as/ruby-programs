def round_time(day, hr, min, sec)
	while sec >= 60
		sec -= 60
		min += 1
	end
	while min >= 60
		min -= 60
		hr += 1
	end
	while hr >= 24
		hr -= 24
		day += 1
	end
	return day, hr, min, sec
end

def add_digits(arg)
	regexp = /\d\d/
	if arg.to_s =~ regexp
		arg = arg.to_s
	else
		arg = "0#{arg}"
	end
	arg
end

def sum_time(*args)
	day = 0
	hr = 00
	min = 00
	sec = 00
	regexp = /(\d\d):(\d\d):(\d\d)/
	args.each do |arr|
		if arr =~ regexp
			newarr = arr.split(":")
			# print newarr
			sec += newarr[2].to_i
			min += newarr[1].to_i
			hr += newarr[0].to_i

			day, hr, min, sec = round_time(day, hr, min, sec)
		else
			puts "#{arr} not correct format: hh:mm:ss" 
		end
	end
	if day != 0
		# print "#{day} day and #{hr}:#{min}:#{sec}"
		print "#{day} day and #{add_digits(hr)}:#{add_digits(min)}:#{add_digits(sec)}"
	else 
		print "#{add_digits(hr)}:#{add_digits(min)}:#{add_digits(sec)}"
	end
end

sum_time("00:45:34","00:15:58", "04:07:09")


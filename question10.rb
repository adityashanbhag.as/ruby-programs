def check(arr)
	len_arr = arr.size
	result = {"odd" => [], "even" => []}

	for i in 0...len_arr
		key = "odd"
		key = "even" if "#{arr[i]}".size.even?

		[arr[i]].inject(result) do |arg, element| 
		    result[key].push(element)
		end

	end
	result
end
val = check(['abc','def',1234,234,'abcd','x','mnop',5,'zZzZ'])
puts val


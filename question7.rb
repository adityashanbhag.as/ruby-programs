class String
	def to_s
		final_char = ""
		self.each_char { |x|
			if x =~ /[A-Z]/
				final_char += x.downcase
			else
				final_char += x.upcase
			end
		}
		return final_char
	end
end
val = "hello WorlD".to_s
print val


class Array
	$dict = {}
	def create_hash(arg1, arg2)
		if $dict.key?(arg1)
			$dict[arg1].push(arg2)
		else
			$dict[arg1] = [arg2]
		end
	end

	def check
		for i in 0...size
			create_hash("#{self[i]}".size, self[i])
		end
		$dict
	end	
end
puts ['abc','def',1234,234,'abcd','x','mnop',5,'zZzZ'].check


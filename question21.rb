def factorial(num)
begin
	raise NegativeNumberException if num.negative?()
	fact = 1
	if num == 0
		return 0
	else
		for i in 1..num
			fact = fact*i
		end
	end
	fact
rescue
	puts "Sorry, you entered negative number"
end
end
puts "enter a number to find its factorial"
puts factorial(gets.chomp.to_i)

